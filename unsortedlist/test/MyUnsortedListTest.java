package datastruct;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MyUnsortedListTest {
    UnsortedList<Integer> list;
    UnsortedList<Integer> emptyList;

    @BeforeEach
    public void init() {
        this.list = MyUnsortedList.of(4,5);
        this.emptyList = MyUnsortedList.of();
    }

    @Test
    public void testInit(){
        assertEquals(2, list.size(), "test la taille d'une liste avec deux elements");
        assertEquals(0, emptyList.size(), "test la taille d'une liste avec 0 elements");
    }


    @Test
    public void isEmptyTest() {
        assertEquals(true, emptyList.isEmpty(), "isEmpty() is false when it should be true");
        assertEquals(false, list.isEmpty(), "isEmpty() is true when it should be false");
    }

    @Test
    public void prependNotEmptyTest() {
        list.prepend(0);
        assertEquals(MyUnsortedList.of(0,4,5), list, "prepend() failed when adding 0 to the list");
    }

    @Test
    public void prependEmptyTest() {
        emptyList.prepend(0);
        assertEquals(MyUnsortedList.of(0), emptyList, "prepend() failed when adding 0 to the empty list");
    }

    @Test
    public void insertAt0Test() {
        list.insert(0, 0);
        assertEquals(MyUnsortedList.of(0,4,5), list, "insert() failed when inserting to pos 0");
    }

    @Test
    public void insertAtEndTest() {
        list.insert(0, list.size());
        assertEquals(MyUnsortedList.of(4,5,0), list, "insert() failed when inserting at the end");
    }

    @Test
    public void insertNegativePosExceptionTest() {
        assertThrows(IndexOutOfBoundsException.class, () -> {list.insert(0, -5);}, "insert() didn't throw an IndexOutOfBoundsException on negative position");
    }

    @Test
    public void insertHigherPosExceptionTest() {
        assertThrows(IndexOutOfBoundsException.class, () -> {list.insert(0, 5000);}, "insert() didn't throw an IndexOutOfBoundsException on position higher than list size");
    }

    @Test
    public void insertMiddleTest() {
        list.insert(0, 1);
        assertEquals(MyUnsortedList.of(4,0,5), list, "insert() failed when inserting in the middle");
    }

    @Test
    public void popLastNonEmptyTest() {
        Integer res = list.popLast();
        assertEquals(Integer.valueOf(5), res, "popLast() returned the wrong element");

        assertEquals(MyUnsortedList.of(4), list, "popLast() failed when removing the last element");
    }

    @Test
    public void popLastEmptyExceptionTest() {
        assertThrows(EmptyListException.class, () -> {emptyList.popLast();}, "popLast() didn't throw an EmptyListException on empty list");
    }

    @Test
    public void TestsizeEmpty(){
        assertEquals(0, emptyList.size(), "test la taille d'une liste vide");
    }

    @Test
    public void TestsizeConst(){
        UnsortedList<Integer> list = MyUnsortedList.of(4,5);
        assertEquals(2, list.size(), "test la taille d'une liste avec deux elements");
    }

    @Test
    public void TestsizeAppend(){
        emptyList.append(5);
        assertEquals(1, emptyList.size(), "test la taille d'une liste avec un append");
    }

    @Test
    public void TestsizePop(){
        list.pop();
        assertEquals(1, list.size(), "test la taille d'une liste avec un pop");
        list.pop();
        assertEquals(0, list.size(), "test la taille d'une liste avec deux pop");
    }

    @Test
    public void testSizeRemove(){
        list.remove(1);
        assertEquals(1, list.size(), "test la taille d'une liste avec un remove");
        list.remove(0);
        assertEquals(0, list.size(), "test la taille d'une liste avec deux remove");
    }

    @Test
    public void testSizePopLast(){
        list.popLast();
        assertEquals(1, list.size(), "test la taille d'une liste avec un popLast");
        list.popLast();
        assertEquals(0, list.size(), "test la taille d'une liste avec deux popLast");
    }
    @Test
    public void testAppendEmptyList(){
        emptyList.append(1);
        assertEquals(MyUnsortedList.of(1), emptyList, "test append sur liste vide");
    }

    @Test
    public void testAppendList(){
        list.append(1);
        assertEquals(MyUnsortedList.of(4,5,1), list, "test append sur liste non vide");
    }

    @Test
    public void testPopEmptyList(){
        assertThrows(EmptyListException.class, () -> {
            emptyList.pop();
        });
    }

    @Test
    public void testPopList(){
        int x = list.pop();
        assertEquals(x, 4, "test return");
        assertEquals(MyUnsortedList.of(5), list, "test pop sur liste non vide");
        x = list.pop();
        assertEquals(x, 5, "test return");
        assertEquals(MyUnsortedList.of(), list, "test pop sur liste non vide");
    }

    @Test
    public void testRemoveEmpty(){
        assertThrows(IndexOutOfBoundsException.class, () -> {
            emptyList.remove(0);
        });
    }

    @Test
    public void testRemoveExceptionNeg(){
        assertThrows(IndexOutOfBoundsException.class, () -> {
            list.remove(-1);
        });
    }

    @Test
    public void testRemoveExceptionSupSive(){
        assertThrows(IndexOutOfBoundsException.class, () -> {
            list.remove(2);
        });
    }

    @Test
    public void testRemoveFirst(){
        int x = list.remove(0);
        assertEquals(x, 4, "test return");
        assertEquals(MyUnsortedList.of(5), list, "test first remove");
    }

    @Test
    public void testRemoveLast(){
        int x = list.remove(1);
        assertEquals(x, 5, "test return");
        assertEquals(MyUnsortedList.of(4), list, "test last remove");
    }

    @Test
    public void testRemove(){
        list.append(3);
        list.append(6);
        int x = list.remove(2);
        assertEquals(x, 3, "test return");
        assertEquals(MyUnsortedList.of(4,5,6), list, "test remove");
    }

    @Test
    public void toStringTest() {
        assertEquals("MyUnsortedList { size = 2, [4, 5] }", list.toString(), "toString() failed for non empty list");
    }

    @Test
    public void toStringEmptyTest() {
        assertEquals("MyUnsortedList { size = 0, [] }", emptyList.toString(), "toString() failed for empty list");
    }

    @Test
    public void equalsDiffObject(){
        assertFalse(list.equals(4), "test la difference d'objet");
    }

    @Test
    public void equalsDiffsize(){
        assertFalse(list.equals(MyUnsortedList.of()), "test taille liste differente");
    }

    @Test
    public void equalsDiffElem(){
        assertFalse(list.equals(MyUnsortedList.of(4,6)), "test des elements differents");
    }
}